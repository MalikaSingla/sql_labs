<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/IDSNlogo.png" width="200" height="200">

# Hands-on Lab: Creating tables, inserting and querying Data in MySQL using phpMyAdmin

**Estimated time needed:** 20 minutes

In this lab, you will learn how to create tables and load data in the MySQL database service using the phpMyAdmin graphical user interface (GUI) tool.

# 

## Software Used in this Lab

In this lab, you will use <a href="https://www.mysql.com/?utm_medium=Exinfluencer&utm_source=Exinfluencer&utm_content=000026UJ&utm_term=10006555&utm_id=NA-SkillsNetwork-Channel-SkillsNetworkCoursesIBMDB0110ENSkillsNetwork24601058-2021-01-01">MySQL</a>. MySQL is a Relational Database Management System (RDBMS) designed to efficiently store, manipulate, and retrieve data.

<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/mysql.png" width="100" height="100">
<p></p>

To complete this lab you will utilize MySQL relational database service available as part of IBM Skills Network Labs (SN Labs) Cloud IDE. SN Labs is a virtual lab environment used in this course.

# 

## Database Used in this Lab

**Mysql_learners** database has been used in this lab.



# 

## Objectives

After completing this lab, you will be able to use phpMyAdmin with MySQL to:

*   Create a database.
*   Create a new table in a database using Create Statement.
*   Insert records into the table.
*   Retreive data from the table
*   Delete an existing table in a database

# 

## Exercise

In this exercise through different tasks, you will learn how to create tables and load data in the MySQL database service using the phpMyAdmin graphical user interface (GUI) tool.

# 

### Task 1: Create a database

1.  Go to **Terminal > New Terminal** to open a terminal from the side by side launched Cloud IDE.

    ![image](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/2.1.png)

<br>

2.  Start MySQL service session in the Cloud IDE using the command below in the terminal. Find your MySQL service session password from the highlighted location of the terminal shown in the image below. Note down your MySQL service session password because you may need to use it later in the lab.

    ```
    start_mysql
    ```

    {: codeblock}

    ![image](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/2.2.png)

<br>

3.  Copy your phpMyAdmin weblink from the highlighted location of the terminal shown in the image below. Past it into the address bar in a new tab of your web browser. This will open the phpMyAdmin tool.

    ![image](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/2.3.png)

<br>

4.  You will see the phpMyAdmin GUI tool.

    ![image](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/2.4.png)

<br>

5.  In the tree-view, click **New** to create a new empty database. Then enter **Mysql_Learners** as the name of the database and click **Create**.

    The encoding will be left as **utf8mb4\_0900\_ai_ci**. UTF-8 is the most commonly used character encoding for content or data.

    Proceed to Task B.

    ![image](images/db1.png)

## Task 2: Create a table in the database

In this step we will create a table in the database with following details:

![image](images/tabledef-instructor.png)

<details>
 <summary>Hint</summary>

Refer CREATE  STATEMENT syntax

```
CREATE TABLE table_name (
column1 datatype,
column2 datatype,
column3 datatype,

);

```

</details>


<details>
<summary>Solution</summary>

```
create table INSTRUCTOR(ID INTEGER PRIMARY KEY NOT NULL, FNAME VARCHAR(20), LNAME VARCHAR(20), CITY VARCHAR(20), CCODE CHAR(2));
```

</details>

<details>
     <summary>Output</summary>

![image](images/4A.png)

</details>

## Task 3: Insert data into the table

In this step we will insert some rows of data into the table.

The INSTRUCTOR table we created in the previous step contains 3 rows of data:

![image](images/rows-instructor.png)

Insert one record first, followed by multiple records.

<details>
 <summary>Hint</summary>

Refer INSERT  STATEMENT syntax for single record insertion

```
INSERT INTO table_name
VALUES (value1, value2, value3, ...);

```
Refer INSERT  STATEMENT syntax for multiple record insertion where we are separating each record insertion by a delimiter which is comma.

```
INSERT INTO table_name
VALUES (value1, value2, value3, ...),
INSERT INTO table_name
VALUES (value1, value2, value3, ...);

```
</details>


<details>
<summary>Solution</summary>

```
insert into INSTRUCTOR values (1, 'Rav', 'Ahuja', 'TORONTO', 'CA');
insert into INSTRUCTOR values (2, 'Raul', 'Chong', 'Markham', 'CA'), (3, 'Hima', 'Vasudevan', 'Chicago', 'US');

```

</details>

<details>
     <summary>Output</summary>

![image](images/4B.png)

</details>

## Task 4: Query data in the table

In this step we will retrieve data we inserted into the INSTRUCTOR table.

<details>
 <summary>Hint</summary>

Refer SELECT  STATEMENT syntax

```
SELECT column1, column2, ...
FROM table_name;

```
To select all records

```
SELECT * 
FROM table_name;
```

</details>


<details>
<summary>Solution</summary>

```
select * from INSTRUCTOR;
```

</details>

<details>
     <summary>Output</summary>

![image](images/4C.png)


</details>

## Task 5: Drop the table.

In this step we will drop the created instructor table.

<details>
<summary>Hint</summary>

Refer DROP table synatx

```
DROP table table_name;

```

</details>


<details>
<summary>Solution</summary>

```
drop table INSTRUCTOR;
```

</details>

<details>
     <summary>Output</summary>

![image](images/4D.png)




</details>

<h3> Congratulations! You have completed this lab, and you are ready for the next topic. <h3/>

<br>

# Author(s)

[Lakshmi Holla](https://www.linkedin.com/in/lakshmi-holla-b39062149/) 


[Malika Singla](https://www.linkedin.com/in/malika-goyal-04798622/)





## <h3 align="center"> © IBM Corporation 2021. All rights reserved. <h3/>