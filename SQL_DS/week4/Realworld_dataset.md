<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/IDSNlogo.png" width="200" height="200">

# Hands-on Lab: Analyzing a real world data-set with SQL in MySQL using phpMyAdmin

**Estimated time needed:** 20 minutes

In this lab, you will learn how to create tables and load data in the MySQL database service using the phpMyAdmin graphical user interface (GUI) tool.

# 

## Software Used in this Lab

In this lab, you will use <a href="https://www.mysql.com/?utm_medium=Exinfluencer&utm_source=Exinfluencer&utm_content=000026UJ&utm_term=10006555&utm_id=NA-SkillsNetwork-Channel-SkillsNetworkCoursesIBMDB0110ENSkillsNetwork24601058-2021-01-01">MySQL</a>. MySQL is a Relational Database Management System (RDBMS) designed to efficiently store, manipulate, and retrieve data.

<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/mysql.png" width="100" height="100">
<p></p>

To complete this lab you will utilize MySQL relational database service available as part of IBM Skills Network Labs (SN Labs) Cloud IDE. SN Labs is a virtual lab environment used in this course.

# 

## Database Used in this Lab

**Mysql_learners** database has been used in this lab.

Firstly create the chicago_socioeconomic_data table using the create table script.

```

CREATE TABLE chicago_socioeconomic_data ( `ca` VARCHAR(2) , 
`community_area_name` VARCHAR(30) NOT NULL , `percent_of_housing_crowded` DOUBLE NOT NULL , 
`percent_households_below_poverty` DOUBLE NOT NULL , `percent_aged_16_unemployed` DOUBLE NOT NULL , 
`percent_aged_25_without_high_school_diploma` DOUBLE NOT NULL , `percent_aged_under_18_or_over_64` DOUBLE NOT NULL , 
`per_capita_income_` BIGINT NOT NULL , `hardship_index` VARCHAR(2)) ;

```

Download the chicago_socioeconomic_data.csv files below to your local computer:

[chicago_socioeconomic_data.csv](https://data.cityofchicago.org/resource/jcxq-k9xf.csv)

Next import the file to the **chicago_socioeconomic_data**
table in the  **Mysql_learners** database and click on **Go**.

![image](images/4E.png)

The csv is loaded.

##### You can verify that the table creation was successful by making a basic query like:

```
SELECT * FROM chicago_socioeconomic_data limit 5;
```
You will get an output as shown below.

![image](images/4F.png)

## Problems

### Problem 1

##### How many rows are in the dataset?

<details><summary>Click here for the solution</summary>

```
SELECT COUNT(*) FROM chicago_socioeconomic_data;

Correct answer: 79
```

</details>

### Problem 2

##### How many community areas in Chicago have a hardship index greater than 50.0?


<details><summary>Click here for the solution</summary>

```
 SELECT COUNT(*) FROM chicago_socioeconomic_data WHERE hardship_index > 50.0;

Correct answer: 38
```

</details>


### Problem 3

##### What is the maximum value of hardship index in this dataset?

<details><summary>Click here for the solution</summary>

```
 SELECT MAX(hardship_index) FROM chicago_socioeconomic_data;

Correct answer: 98.0
```

</details>

### Problem 4

##### Which community area which has the highest hardship index?


<details><summary>Click here for the solution</summary>

```
#We can use the result of the last query to as an input to this query:

 SELECT community_area_name FROM chicago_socioeconomic_data where hardship_index=98.0

#or another option:
 SELECT community_area_name FROM chicago_socioeconomic_data ORDER BY hardship_index DESC LIMIT 1;

#or you can use a sub-query to determine the max hardship index:
 select community_area_name from chicago_socioeconomic_data where hardship_index = ( select max(hardship_index) from chicago_socioeconomic_data ) 

Correct answer: 'Riverdale'
    
```

</details>

### Problem 5

##### Which Chicago community areas have per-capita incomes greater than $60,000?

<details><summary>Click here for the solution</summary>

```
 SELECT community_area_name FROM chicago_socioeconomic_data WHERE per_capita_income_ > 60000;

Correct answer:Lake View,Lincoln Park, Near North Side, Loop
    
```

</details>


# Author(s)

[Lakshmi Holla](https://www.linkedin.com/in/lakshmi-holla-b39062149/) 


[Malika Singla](https://www.linkedin.com/in/malika-goyal-04798622/)





## <h3 align="center"> © IBM Corporation 2021. All rights reserved. <h3/>