
CREATE TABLE chicago_socioeconomic_data ( `ca` VARCHAR(2) , 
`community_area_name` VARCHAR(30) NOT NULL , `percent_of_housing_crowded` DOUBLE NOT NULL , 
`percent_households_below_poverty` DOUBLE NOT NULL , `percent_aged_16_unemployed` DOUBLE NOT NULL , 
`percent_aged_25_without_high_school_diploma` DOUBLE NOT NULL , `percent_aged_under_18_or_over_64` DOUBLE NOT NULL , 
`per_capita_income_` BIGINT NOT NULL , `hardship_index` VARCHAR(2)) ;







Preview SQL

CREATE TABLE `chicago_public_schools` ( `School_ID` VARCHAR(30) NOT NULL , `NAME_OF_SCHOOL` VARCHAR(100) NOT NULL ,
 `Elementary, Middle, or High School` VARCHAR(100) NOT NULL , `Street_Address` VARCHAR(200) NOT NULL , `City` VARCHAR(100) NOT NULL ,
 `State` VARCHAR(200) NOT NULL , `HEALTHY_SCHOOL_CERTIFIED` VARCHAR(30) NOT NULL , `Safety_Icon` VARCHAR(30) NOT NULL , `SAFETY_SCORE` INT(11) , 
`Leaders_Icon` VARCHAR(30) NOT NULL , `Leaders_Score` VARCHAR(30) NOT NULL , `AVERAGE_STUDENT_ATTENDANCE` VARCHAR(30) NOT NULL , `COLLEGE_ENROLLMENT` VARCHAR(20) NOT NULL ,
 `X_COORDINATE` VARCHAR(30) NOT NULL , `Y_COORDINATE` VARCHAR(30) NOT NULL , `Latitude` VARCHAR(20) NOT NULL , `Longitude` VARCHAR(20) NOT NULL , 
`COMMUNITY_AREA_NUMBER` VARCHAR(30) NOT NULL , 

`COMMUNITY_AREA_NAME` VARCHAR(100) NOT NULL , 
`Ward` VARCHAR(40) NOT NULL , `Police_District` VARCHAR(100) NOT NULL , `Location` VARCHAR(100) NOT NULL ) ;
