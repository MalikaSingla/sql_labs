<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/IDSNlogo.png" width="200" height="200">

# Hands-on Lab:Peer graded assignment using SQL in MySQL using phpMyAdmin

**Estimated time needed:** 20 minutes

In this lab, you will learn how to create tables and load data in the MySQL database service using the phpMyAdmin graphical user interface (GUI) tool.

# 

## Software Used in this Lab

In this lab, you will use <a href="https://www.mysql.com/?utm_medium=Exinfluencer&utm_source=Exinfluencer&utm_content=000026UJ&utm_term=10006555&utm_id=NA-SkillsNetwork-Channel-SkillsNetworkCoursesIBMDB0110ENSkillsNetwork24601058-2021-01-01">MySQL</a>. MySQL is a Relational Database Management System (RDBMS) designed to efficiently store, manipulate, and retrieve data.

<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/mysql.png" width="100" height="100">
<p></p>

To complete this lab you will utilize MySQL relational database service available as part of IBM Skills Network Labs (SN Labs) Cloud IDE. SN Labs is a virtual lab environment used in this course.

# 

## Database Used in this Lab

**Mysql_learners** database has been used in this lab.

Here you will be creating 3 tables

1.chicago_public_schools
2.chicago_socioeconomic_data
3.chicago_crime



Firstly create the chicago_public_schools table using the create table script .

Copy paste the given script in the **sql query** editor and click on **Go**.

```

CREATE TABLE `chicago_public_schools` ( `School_ID` VARCHAR(30) NOT NULL , `NAME_OF_SCHOOL` VARCHAR(100) NOT NULL ,
 `Elementary, Middle, or High School` VARCHAR(100) NOT NULL , `Street_Address` VARCHAR(200) NOT NULL , `City` VARCHAR(100) NOT NULL ,
 `State` VARCHAR(200) NOT NULL , `HEALTHY_SCHOOL_CERTIFIED` VARCHAR(30) NOT NULL , `Safety_Icon` VARCHAR(30) NOT NULL , `SAFETY_SCORE` INT(11) , 
`Leaders_Icon` VARCHAR(30) NOT NULL , `Leaders_Score` VARCHAR(30) NOT NULL , `AVERAGE_STUDENT_ATTENDANCE` VARCHAR(30) NOT NULL , `COLLEGE_ENROLLMENT` VARCHAR(20) NOT NULL ,
 `X_COORDINATE` VARCHAR(30) NOT NULL , `Y_COORDINATE` VARCHAR(30) NOT NULL , `Latitude` VARCHAR(20) NOT NULL , `Longitude` VARCHAR(20) NOT NULL , 
`COMMUNITY_AREA_NUMBER` VARCHAR(30) NOT NULL , 
`COMMUNITY_AREA_NAME` VARCHAR(100) NOT NULL , 
`Ward` VARCHAR(40) NOT NULL , `Police_District` VARCHAR(100) NOT NULL , `Location` VARCHAR(100) NOT NULL ) ;

```

Once the table is created,download the ChicagoPublicSchools.csv files below to your local computer:

[ChicagoPublicSchools.csv](ChicagoPublicSchools.csv)

Next import the file to the **chicago_public_schools**
table in the  **Mysql_learners** database and click on **Go**.

> Note:While  importing change the value to 1 at
Skip this number of queries (for SQL) starting from the first one:

![image](images/4E.png)


The csv is loaded.

Next create the chicago_socioeconomic_data table using the create table script.
Copy paste the given script in the **sql query** editor and click on **Go**.
```

CREATE TABLE chicago_socioeconomic_data ( `ca` VARCHAR(2) , 
`community_area_name` VARCHAR(30) NOT NULL , `percent_of_housing_crowded` DOUBLE NOT NULL , 
`percent_households_below_poverty` DOUBLE NOT NULL , `percent_aged_16_unemployed` DOUBLE NOT NULL , 
`percent_aged_25_without_high_school_diploma` DOUBLE NOT NULL , `percent_aged_under_18_or_over_64` DOUBLE NOT NULL , 
`per_capita_income_` BIGINT NOT NULL , `hardship_index` VARCHAR(2)) ;

```

Download the chicago_socioeconomic_data.csv files below to your local computer:

[chicago_socioeconomic_data.csv](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-DB0201EN-SkillsNetwork/labs/FinalModule_Coursera_V5/data/ChicagoCensusData.csv)

Next import the file to the **chicago_socioeconomic_data**
table in the  **Mysql_learners** database and click on **Go**.

![image](images/4F.png)


Next create the chicago_crime table using the following create script.
Copy paste the given script in the **sql query** editor and click on **Go**.

```
CREATE TABLE `chicago_crime` ( `ID` BIGINT NOT NULL , 
`CASE_NUMBER` VARCHAR(30) NOT NULL , `DATE` DATE NOT NULL , 
`BLOCK` VARCHAR(50) NOT NULL , `IUCR` VARCHAR(40) , 
`PRIMARY_TYPE` VARCHAR(50) NOT NULL , `DESCRIPTION` VARCHAR(100) NOT NULL , 
`LOCATION_DESCRIPTION` VARCHAR(200) NOT NULL , `ARREST` VARCHAR(50) NOT NULL , 
`DOMESTIC` VARCHAR(30) NOT NULL , `BEAT` BIGINT NOT NULL , `DISTRICT` INT NOT NULL ,
 `WARD` VARCHAR(40) , `COMMUNITY_AREA_NUMBER` VARCHAR(40) , `FBICODE` VARCHAR(40) , 
`X_COORDINATE` VARCHAR(30) NOT NULL , `Y_COORDINATE` VARCHAR(30) NOT NULL , `YEAR` INT NOT NULL , `LATITUDE` VARCHAR(40) NOT NULL , `LONGITUDE` VARCHAR(40) NOT NULL , `LOCATION` VARCHAR(100) NOT NULL ) ;

   
```

Download the chicago_crime.csv files below to your local computer:

[ChicagoCrimeData.csv](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-DB0201EN-SkillsNetwork/labs/FinalModule_Coursera_V5/data/ChicagoCrimeData.csv)
)
Next import the file to the **chicago_crime**
table in the  **Mysql_learners** database and click on **Go**.



![image](images/4G.png)


## Problems

Now write and execute SQL queries to solve assignment problems

### Problem 1

##### Find the total number of crimes recorded in the CRIME table.


```
select COUNT(*) from chicago_crime;

```

![image](images/F1.png)


### Problem 2

##### List community areas with per capita income less than 11000.


```
select community_area_name,per_capita_income_ from chicago_socioeconomic_data where per_capita_income_ < 11000


```
![image](images/F2.png)


### Problem 3

##### List all case numbers for crimes  involving minors?(children are not considered minors for the purposes of crime analysis)

```
select case_number from chicago_crime where description like '%MINOR%'

```
![image](images/F3.png)


### Problem 4

##### List all kidnapping crimes involving a child?


```

select * from chicago_crime  where description like '%child%'  and  PRIMARY_TYPE='KIDNAPPING'


```

![image](images/F4.png)

### Problem 5

##### What kinds of crimes were recorded at schools?


```
select DISTINCT(primary_type) from chicago_crime  where location_description like '%SCHOOL%'


```
![image](images/F5.png)

### Problem 6

##### List the average safety score for all types of schools.


```

select `Elementary, Middle, or High School`, AVG(SAFETY_SCORE) from chicago_public_schools GROUP BY `Elementary, Middle, or High School`;

```

![image](images/F6.png)


### Problem 7

##### List 5 community areas with highest % of households below poverty line

```
SELECT COMMUNITY_AREA_NAME, PERCENT_HOUSEHOLDS_BELOW_POVERTY 
FROM chicago_socioeconomic_data ORDER BY PERCENT_HOUSEHOLDS_BELOW_POVERTY DESC LIMIT 5;

```


![image](images/F7.png)


### Problem 8

##### Which community area is most crime prone?


```

SELECT COMMUNITY_AREA_NUMBER, COUNT(COMMUNITY_AREA_NUMBER) FROM chicago_crime GROUP BY COMMUNITY_AREA_NUMBER
ORDER BY COUNT(COMMUNITY_AREA_NUMBER) DESC LIMIT 1;

```


![image](images/F8.png)


### Problem 9

##### Use a sub-query to find the name of the community area with highest hardship index

```

SELECT DISTINCT(COMMUNITY_AREA_NAME) FROM chicago_socioeconomic_data WHERE HARDSHIP_INDEX = (SELECT MAX(HARDSHIP_INDEX) FROM chicago_socioeconomic_data);
```
![image](images/F9.png)

### Problem 10

##### Use a sub-query to determine the Community Area Name with most number of crimes?


```

select DISTINCT(COMMUNITY_AREA_NAME) FROM chicago_socioeconomic_data
WHERE ca=(SELECT COMMUNITY_AREA_NUMBER  from chicago_crime
                             GROUP BY COMMUNITY_AREA_NUMBER  ORDER BY COUNT(*) DESC LIMIT 1)

```
![image](images/F10.png)



# Author(s)

[Lakshmi Holla](https://www.linkedin.com/in/lakshmi-holla-b39062149/) 


[Malika Singla](https://www.linkedin.com/in/malika-goyal-04798622/)





## <h3 align="center"> © IBM Corporation 2021. All rights reserved. <h3/>