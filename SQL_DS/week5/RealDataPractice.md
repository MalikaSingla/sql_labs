<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/IDSNlogo.png" width="200" height="200">

# Hands-on Lab:Working with a real world data-set using SQL in MySQL using phpMyAdmin

**Estimated time needed:** 20 minutes

In this lab, you will learn how to create tables and load data in the MySQL database service using the phpMyAdmin graphical user interface (GUI) tool.

# 

## Software Used in this Lab

In this lab, you will use <a href="https://www.mysql.com/?utm_medium=Exinfluencer&utm_source=Exinfluencer&utm_content=000026UJ&utm_term=10006555&utm_id=NA-SkillsNetwork-Channel-SkillsNetworkCoursesIBMDB0110ENSkillsNetwork24601058-2021-01-01">MySQL</a>. MySQL is a Relational Database Management System (RDBMS) designed to efficiently store, manipulate, and retrieve data.

<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/mysql.png" width="100" height="100">
<p></p>

To complete this lab you will utilize MySQL relational database service available as part of IBM Skills Network Labs (SN Labs) Cloud IDE. SN Labs is a virtual lab environment used in this course.

# 

## Database Used in this Lab

**Mysql_learners** database has been used in this lab.

Firstly create the chicago_public_schools table using the create table script .

Copy paste the given script in the **sql query** editor and click on **Go**.

```


CREATE TABLE `chicago_public_schools` ( `School_ID` VARCHAR(30) NOT NULL , `NAME_OF_SCHOOL` VARCHAR(100) NOT NULL ,
 `Elementary, Middle, or High School` VARCHAR(100) NOT NULL , `Street_Address` VARCHAR(200) NOT NULL , `City` VARCHAR(100) NOT NULL ,
 `State` VARCHAR(200) NOT NULL , `HEALTHY_SCHOOL_CERTIFIED` VARCHAR(30) NOT NULL , `Safety_Icon` VARCHAR(30) NOT NULL , `SAFETY_SCORE` INT(11) , 
`Leaders_Icon` VARCHAR(30) NOT NULL , `Leaders_Score` VARCHAR(30) NOT NULL , `AVERAGE_STUDENT_ATTENDANCE` VARCHAR(30) NOT NULL , `COLLEGE_ENROLLMENT` VARCHAR(20) NOT NULL ,
 `X_COORDINATE` VARCHAR(30) NOT NULL , `Y_COORDINATE` VARCHAR(30) NOT NULL , `Latitude` VARCHAR(20) NOT NULL , `Longitude` VARCHAR(20) NOT NULL , 
`COMMUNITY_AREA_NUMBER` VARCHAR(30) NOT NULL , 
`COMMUNITY_AREA_NAME` VARCHAR(100) NOT NULL , 
`Ward` VARCHAR(40) NOT NULL , `Police_District` VARCHAR(100) NOT NULL , `Location` VARCHAR(100) NOT NULL ) ;

```

Once the table is created,download the ChicagoPublicSchools.csv files below to your local computer:

[ChicagoPublicSchools.csv](ChicagoPublicSchools.csv)

Next import the file to the **chicago_public_schools**
table in the  **Mysql_learners** database and click on **Go**.

> Note:While  importing change the value to 1 at
Skip this number of queries (for SQL) starting from the first one:

![image](images/4E.png)


The csv is loaded.

Next create the chicago_socioeconomic_data table using the create table script.

```

CREATE TABLE chicago_socioeconomic_data ( `ca` VARCHAR(2) , 
`community_area_name` VARCHAR(30) NOT NULL , `percent_of_housing_crowded` DOUBLE NOT NULL , 
`percent_households_below_poverty` DOUBLE NOT NULL , `percent_aged_16_unemployed` DOUBLE NOT NULL , 
`percent_aged_25_without_high_school_diploma` DOUBLE NOT NULL , `percent_aged_under_18_or_over_64` DOUBLE NOT NULL , 
`per_capita_income_` BIGINT NOT NULL , `hardship_index` VARCHAR(2)) ;

```

Download the chicago_socioeconomic_data.csv files below to your local computer:

[chicago_socioeconomic_data.csv](https://data.cityofchicago.org/resource/jcxq-k9xf.csv)

Next import the file to the **chicago_socioeconomic_data**
table in the  **Mysql_learners** database and click on **Go**.

![image](images/4F.png)

## Problems

### Problem 1

##### How many Elementary Schools are in the dataset?

Double-click **here** for a hint

<!--
Which column specifies the school type e.g. 'ES', 'MS', 'HS'? ("Elementary School, Middle School, High School")
-->

Double-click **here** for another hint

<!--
Does the column name have mixed case, spaces or other special characters?
If so, ensure you use ` symbol around the `Name of the Column`
-->
Double-click **here** for the solution.

<!-- Solution:

select count(*) from chicago_public_schools where `Elementary, Middle, or High School` ='ES'

Correct answer: 462

-->

### Problem 2

##### What is the highest Safety Score?

Double-click **here** for a hint

<!--
Use the MAX() function
-->
Double-click **here** for the solution.

<!-- Hint:


select MAX(Safety_Score) AS MAX_SAFETY_SCORE from chicago_public_schools;

Correct answer: 99
-->
### Problem 3

##### Which schools have highest Safety Score?

Double-click **here** for the solution.

<!-- Solution:
In the previous problem we found out that the highest Safety Score is 99, so we can use that as an input in the where clause:

select Name_of_School, Safety_Score from chicago_public_schools where Safety_Score = 99


or, a better way:

select Name_of_School, Safety_Score from chicago_public_schools where 
  Safety_Score= (select MAX(Safety_Score) from chicago_public_schools)

Correct answer: several schools with with Safety Score of 99.
-->
### Problem 4

##### What are the top 10 schools with the highest "Average Student Attendance"?

Double-click **here** for the solution.

<!-- Solution:

select Name_of_School, Average_Student_Attendance from chicago_public_schools order by Average_Student_Attendance desc limit 10; 

-->

### Problem 5

##### Retrieve the list of 5 Schools with the lowest Average Student Attendance sorted in ascending order based on attendance

Double-click **here** for the solution.

<!-- Solution:


SELECT Name_of_School, Average_Student_Attendance  
     from chicago_public_schools 
     order by Average_Student_Attendance 
     LIMIT 5;

-->


### Problem 6

##### Now remove the '%' sign from the above result set for Average Student Attendance column

Double-click **here** for a hint

<!--
Use the REPLACE() function to replace '%' with ''
See documentation for this function at:
https://www.ibm.com/support/knowledgecenter/en/SSEPGG_10.5.0/com.ibm.db2.luw.sql.ref.doc/doc/r0000843.html
-->
Double-click **here** for the solution.

<!-- Hint:

SELECT Name_of_School, REPLACE(Average_Student_Attendance, '%', '') 
     from chicago_public_schools
     order by Average_Student_Attendance 
    LIMIT 5
-->

### Problem 7

##### Which Schools have Average Student Attendance lower than 70%?

Double-click **here** for a hint

<!--
The datatype of the "Average_Student_Attendance" column is varchar.
So you cannot use it as is in the where clause for a numeric comparison.
First use the CAST() function to cast it as a DECIMAL or DOUBLE
e.g. CAST("Column_Name" as DOUBLE)
-->

Double-click **here** for another hint

<!--
Don't forget the '%' age sign needs to be removed before casting
-->

Double-click **here** for the solution.

<!-- Solution:

SELECT Name_of_School, Average_Student_Attendance from chicago_public_schools
    where CAST(REPLACE(Average_Student_Attendance, '%', '')  AS DOUBLE) < 70
    order by Average_Student_Attendance
     -->


### Problem 8

##### Get the total College Enrollment for each Community Area

Double-click **here** for a hint

<!--
Verify the exact name of the Enrollment column in the database
Use the SUM() function to add up the Enrollments for each Community Area
-->
Double-click **here** for another hint

<!--
Don't forget to group by the Community Area
-->
Double-click **here** for the solution.

<!-- Solution:


select Community_Area_Name, sum(College_Enrollment) AS TOTAL_ENROLLMENT 
   from chicago_public_schools 
   group by Community_Area_Name 


-->

### Problem 9

##### Get the 5 Community Areas with the least total College Enrollment  sorted in ascending order

Double-click **here** for a hint

<!--
Order the previous query and limit the number of rows you fetch
-->

Double-click **here** for the solution.

<!-- Solution:


select Community_Area_Name, sum(College_Enrollment) AS TOTAL_ENROLLMENT 
   from chicago_public_schools 
   group by Community_Area_Name 
   order by TOTAL_ENROLLMENT asc 
   LIMIT 5

-->

### Problem 10

##### List 5 schools with lowest safety score.

Double-click **here** for the solution.

<!-- Solution:

SELECT name_of_school, safety_score
FROM chicago_public_schools
ORDER BY safety_score
LIMIT 5
-->

### Problem 11

##### Get the hardship index for the community area which has College Enrollment of 4368

Double-click **here** for the solution.

<!-- Solution:
NOTE: For this solution to work the CHICAGO_SOCIOECONOMIC_DATA table 
      as created in the last lab of Week 3 should already exist

select hardship_index 
   from chicago_socioeconomic_data CD, chicago_public_schools CPS 
   where CD.ca = CPS.community_area_number 
      and college_enrollment = 4368


-->
### Problem 12

##### Get the hardship index for the community area which has the school with the  highest enrollment.

Double-click **here** for the solution.

<!-- Solution:

select ca, community_area_name, hardship_index from chicago_socioeconomic_data 
   where ca in 
   ( select community_area_number from chicago_public_schools where COLLEGE_ENROLLMENT = (select MAX(COLLEGE_ENROLLMENT) from chicago_public_schools)  ) ;


-->


# Author(s)

[Lakshmi Holla](https://www.linkedin.com/in/lakshmi-holla-b39062149/) 


[Malika Singla](https://www.linkedin.com/in/malika-goyal-04798622/)





## <h3 align="center"> © IBM Corporation 2021. All rights reserved. <h3/>