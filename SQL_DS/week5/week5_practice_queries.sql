select count(*) from chicago_public_schools where `Elementary, Middle, or High School` ='ES'

select MAX(Safety_Score) AS MAX_SAFETY_SCORE from chicago_public_schools;

select Name_of_School, Safety_Score from chicago_public_schools where Safety_Score = 99

select Name_of_School, Safety_Score from chicago_public_schools where 
  Safety_Score= (select MAX(Safety_Score) from chicago_public_schools)

select Name_of_School, Average_Student_Attendance from chicago_public_schools order by Average_Student_Attendance desc limit 10; 


SELECT Name_of_School, Average_Student_Attendance  
     from chicago_public_schools 
     order by Average_Student_Attendance 
     LIMIT 5;

SELECT Name_of_School, REPLACE(Average_Student_Attendance, '%', '') 
     from chicago_public_schools
     order by Average_Student_Attendance 
    LIMIT 5


SELECT Name_of_School, Average_Student_Attendance from chicago_public_schools
    where CAST(REPLACE(Average_Student_Attendance, '%', '')  AS DOUBLE) < 70
    order by Average_Student_Attendance

select Community_Area_Name, sum(College_Enrollment) AS TOTAL_ENROLLMENT 
   from chicago_public_schools 
   group by Community_Area_Name 


select Community_Area_Name, sum(College_Enrollment) AS TOTAL_ENROLLMENT 
   from chicago_public_schools 
   group by Community_Area_Name 
   order by TOTAL_ENROLLMENT asc 
   LIMIT 5


SELECT name_of_school, safety_score
FROM chicago_public_schools
ORDER BY safety_score
LIMIT 5


select hardship_index 
   from chicago_socioeconomic_data CD, chicago_public_schools CPS 
   where CD.ca = CPS.community_area_number 
      and college_enrollment = 4368


   select ca, community_area_name, hardship_index from chicago_socioeconomic_data 
   where ca in 
   ( select community_area_number from chicago_public_schools where COLLEGE_ENROLLMENT = (select MAX(COLLEGE_ENROLLMENT) from chicago_public_schools)  ) ;








CREATE TABLE `chicago_crime` ( `ID` BIGINT NOT NULL , 
`CASE_NUMBER` VARCHAR(30) NOT NULL , `DATE` DATE NOT NULL , 
`BLOCK` VARCHAR(50) NOT NULL , `IUCR` VARCHAR(40) , 
`PRIMARY_TYPE` VARCHAR(50) NOT NULL , `DESCRIPTION` VARCHAR(100) NOT NULL , 
`LOCATION_DESCRIPTION` VARCHAR(200) NOT NULL , `ARREST` VARCHAR(50) NOT NULL , 
`DOMESTIC` VARCHAR(30) NOT NULL , `BEAT` BIGINT NOT NULL , `DISTRICT` INT NOT NULL ,
 `WARD` VARCHAR(40) , `COMMUNITY_AREA_NUMBER` VARCHAR(40) , `FBICODE` VARCHAR(40) , 
`X_COORDINATE` VARCHAR(30) NOT NULL , `Y_COORDINATE` VARCHAR(30) NOT NULL , `YEAR` INT NOT NULL , `LATITUDE` VARCHAR(40) NOT NULL , `LONGITUDE` VARCHAR(40) NOT NULL , `LOCATION` VARCHAR(100) NOT NULL ) ;
   


