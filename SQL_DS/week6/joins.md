<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/IDSNlogo.png" width="200" height="200">

# Hands-on Lab: Working with Multiple Tables in MySQL using phpMyAdmin

**Estimated time needed:** 20 minutes

In this lab, you will learn how to create tables and load data in the MySQL database service using the phpMyAdmin graphical user interface (GUI) tool.

# 

## Software Used in this Lab

In this lab, you will use <a href="https://www.mysql.com/?utm_medium=Exinfluencer&utm_source=Exinfluencer&utm_content=000026UJ&utm_term=10006555&utm_id=NA-SkillsNetwork-Channel-SkillsNetworkCoursesIBMDB0110ENSkillsNetwork24601058-2021-01-01">MySQL</a>. MySQL is a Relational Database Management System (RDBMS) designed to efficiently store, manipulate, and retrieve data.

<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/mysql.png" width="100" height="100">
<p></p>

To complete this lab you will utilize MySQL relational database service available as part of IBM Skills Network Labs (SN Labs) Cloud IDE. SN Labs is a virtual lab environment used in this course.

# 

## Database Used in this Lab

The database used in this lab is an internal database. You will be working on a sample HR database. This HR database schema consists of 5 tables called **EMPLOYEES**, **JOB_HISTORY**, **JOBS**, **DEPARTMENTS** and **LOCATIONS**. Each table has a few rows of sample data. The following diagram shows the tables for the HR database:

<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-DB0201EN-SkillsNetwork/labs/Labs_Coursera_V5/labs/Lab%20-%20Create%20tables%20using%20SQL%20scripts%20and%20Load%20data%20into%20tables/images/Sample_1.PNG" width="670" height="400">

In this lab, you will run through some SQL practice problems that will provide hands-on experience with the different kinds of join operations.

<br>

**How does a CROSS JOIN (also known as Cartesian Join) statement syntax look?**

```
SELECT column_name(s)
FROM table1
CROSS JOIN table2;
```

<br>

**How does an INNER JOIN statement syntax look?**

```
SELECT column_name(s)
FROM table1
INNER JOIN table2
ON table1.column_name = table2.column_name;
WHERE condition;
```

<br>

**How does a LEFT OUTER JOIN statement syntax look?**

```
SELECT column_name(s)
FROM table1
LEFT OUTER JOIN table2
ON table1.column_name = table2.column_name
WHERE condition;
```

<br>

**How does a RIGHT OUTER JOIN statement syntax look?**

```
SELECT column_name(s)
FROM table1
RIGHT OUTER JOIN table2
ON table1.column_name = table2.column_name
WHERE condition;
```

<br>

**How does a FULL OUTER JOIN statement syntax look?**

```
SELECT column_name(s)
FROM table1
LEFT  OUTER JOIN table2
ON table1.column_name = table2.column_name
WHERE condition

UNION

SELECT column_name(s)
FROM table1
RIGHT  OUTER JOIN table2
ON table1.column_name = table2.column_name
WHERE condition
```
**Union operator**

The UNION operator is used to combine the result-set of two or more SELECT statements.

Every SELECT statement within UNION must have the same number of columns
The columns must also have similar data types
The columns in every SELECT statement must also be in the same order

```
SELECT column_name(s) FROM table1
UNION
SELECT column_name(s) FROM table2;

```

<br>

**How does a SELF JOIN statement syntax look?**

```
SELECT column_name(s)
FROM table1 T1, table1 T2
WHERE condition;
```

<br>
 # Exercise

1.  Problem:

    > *Select the names and job start dates of all employees who work for the department number 5.*

     <details>
     <summary>Hint</summary>

    > Use the Inner join operation with the EMPLOYEES table as the left table and the JOB_HISTORY table as the right table.

     </details>

     <details>
     <summary>Solution</summary>

    ```
    select E.F_NAME,E.L_NAME, JH.START_DATE 
    from EMPLOYEES as E 
    INNER JOIN JOB_HISTORY as JH on E.EMP_ID=JH.EMPL_ID 
    where E.DEP_ID ='5';	
    ```

     </details>

     <details>
     <summary>Output</summary>

    ![image](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-DB0201EN-SkillsNetwork/labs/Labs_Coursera_V5/labs/Lab%20-%20Joins/images/1.A.png)

     </details>

2.  Problem:

    > *Select the names, job start dates, and job titles of all employees who work for the department number 5.*

     <details>
     <summary>Hint</summary>

    > Perform an INNER JOIN with 3 tables â€“ EMPLOYEES, JOB_HISTORY, JOBS.

     </details>

     <details>
     <summary>Solution</summary>

    ```
    select E.F_NAME,E.L_NAME, JH.START_DATE, J.JOB_TITLE 
    from EMPLOYEES as E 
    INNER JOIN JOB_HISTORY as JH on E.EMP_ID=JH.EMPL_ID 
    INNER JOIN JOBS as J on E.JOB_ID=J.JOB_IDENT
    where E.DEP_ID ='5';
    ```

     </details>

     <details>
     <summary>Output</summary>

    ![image](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-DB0201EN-SkillsNetwork/labs/Labs_Coursera_V5/labs/Lab%20-%20Joins/images/1.B.png)

     </details>

3.  Problem:

    > *Perform a Left Outer Join on the EMPLOYEES and DEPARTMENT tables and select employee id, last name, department id and department name for all employees.*

     <details>
     <summary>Hint</summary>

    > Use the Left Outer Join operation with the EMPLOYEES table as the left table and the DEPARTMENTS table as the right table.

     </details>

     <details>
     <summary>Solution</summary>

    ```
    select E.EMP_ID,E.L_NAME,E.DEP_ID,D.DEP_NAME
    from EMPLOYEES AS E 
    LEFT OUTER JOIN DEPARTMENTS AS D ON E.DEP_ID=D.DEPT_ID_DEP;
    ```

     </details>

     <details>
     <summary>Output</summary>

    ![image](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-DB0201EN-SkillsNetwork/labs/Labs_Coursera_V5/labs/Lab%20-%20Joins/images/2.A.png)

     </details>

4.  Problem:

    > *Re-write the previous query but limit the result set to include only the rows for employees born before 1980.*

     <details>
     <summary>Hint</summary>

    > Use a WHERE clause and Left Outer Join operation. Alternatively, you could also use an INNER JOIN.

     </details>

     <details>
     <summary>Solution</summary>

    ```
    select E.EMP_ID,E.L_NAME,E.DEP_ID,D.DEP_NAME
    from EMPLOYEES AS E 
    LEFT OUTER JOIN DEPARTMENTS AS D ON E.DEP_ID=D.DEPT_ID_DEP 
    where YEAR(E.B_DATE) < 1980;
    ```

     </details>

     <details>
     <summary>Output</summary>

    ![image](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-DB0201EN-SkillsNetwork/labs/Labs_Coursera_V5/labs/Lab%20-%20Joins/images/2.B.png)

     </details>

5.  Problem:

    > *Re-write the previous query but have the result set include all the employees but department names for only the employees who were born before 1980.*

     <details>
     <summary>Hint</summary>

    > Use an AND in the LEFT OUTER JOIN clause.

     </details>

     <details>
     <summary>Solution</summary>

    ```
    select E.EMP_ID,E.L_NAME,E.DEP_ID,D.DEP_NAME
    from EMPLOYEES AS E 
    LEFT OUTER JOIN DEPARTMENTS AS D ON E.DEP_ID=D.DEPT_ID_DEP 
    AND YEAR(E.B_DATE) < 1980;
    ```

     </details>

     <details>
     <summary>Output</summary>

    ![image](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-DB0201EN-SkillsNetwork/labs/Labs_Coursera_V5/labs/Lab%20-%20Joins/images/2.C.png)

     </details>

6.  Problem:

    > *Perform a Full Join on the EMPLOYEES and DEPARTMENT tables and select the First name, Last name and Department name of all employees.*

     <details>
     <summary>Hint</summary>

    > Use the Full Outer Join operation with the EMPLOYEES table as the left table and the DEPARTMENTS table as the right table.

     </details>

     <details>
     <summary>Solution</summary>

    ```
    select E.F_NAME,E.L_NAME,D.DEP_NAME
	from EMPLOYEES AS E 
    LEFT OUTER JOIN DEPARTMENTS AS D ON E.DEP_ID=D.DEPT_ID_DEP

    UNION

    select E.F_NAME,E.L_NAME,D.DEP_NAME
	from EMPLOYEES AS E 
    RIGHT OUTER JOIN DEPARTMENTS AS D ON E.DEP_ID=D.DEPT_ID_DEP
    ```

     </details>

     <details>
     <summary>Output</summary>

    ![image](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-DB0201EN-SkillsNetwork/labs/Labs_Coursera_V5/labs/Lab%20-%20Joins/images/3.A.png)

     </details>

7.  Problem:

    > *Re-write the previous query but have the result set include all employee names but department id and department names only for male employees.*

     <details>
     <summary>Hint</summary>

    > Add an AND in Query 3A to filter on male employees in the ON clause. Alternatively, you can also use Left Outer Join.

     </details>

     <details>
     <summary>Solution</summary>

    ```
    select E.F_NAME,E.L_NAME,D.DEPT_ID_DEP, D.DEP_NAME
	from EMPLOYEES AS E 
	LEFT OUTER JOIN DEPARTMENTS AS D ON E.DEP_ID=D.DEPT_ID_DEP AND E.SEX = 'M'


    UNION

    select E.F_NAME,E.L_NAME,D.DEPT_ID_DEP, D.DEP_NAME
	from EMPLOYEES AS E 
	RIGHT OUTER JOIN DEPARTMENTS AS D ON E.DEP_ID=D.DEPT_ID_DEP AND E.SEX = 'M';
    ```

     </details>

     <details>
     <summary>Output</summary>

    ![image](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-DB0201EN-SkillsNetwork/labs/Labs_Coursera_V5/labs/Lab%20-%20Joins/images/3.B.png)

     </details>

<br>

# Solution Script

If you would like to run all the solution queries of the SQL problems of this lab with a script, download the script below. Import the script to mysql phpadmin interface. Follow [Hands-on Lab : Create tables using SQL scripts and Load data into tables](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-DB0201EN-SkillsNetwork/labs/Labs_Coursera_V5/labs/Lab%20-%20Create%20tables%20using%20SQL%20scripts%20and%20Load%20data%20into%20tables/instructional-labs.md.html) on how to import a script to mysql phpadmin interface.

*   [JOIN_Solution_Script.sql](JOIN_Solution_Script.sql)

<br>

<h3> Congratulations! You have completed this lab, and you are ready for the next topic. <h3/>

<br>

# Author(s)

[Lakshmi Holla](https://www.linkedin.com/in/lakshmi-holla-b39062149/) 

[Malika Singla](https://www.linkedin.com/in/malika-goyal-04798622/)


## <h3 align="center"> © IBM Corporation 2021. All rights reserved. <h3/>
